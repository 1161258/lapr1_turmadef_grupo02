package LAPR1_Final_Project;

import static LAPR1_Final_Project.Common.isDouble;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

/**
 *
 * @author Vitor Melo
 * @author Vitor Cardoso
 * @author Filipe Ferreira
 * @author Guilherme Oliveira
 */
public class Topsis {

    /**
     * Modulo que vai tratar do output do programa (tanto para ficheiro como
     * para a consola )
     *
     * @param vetorPesos vetor contendo os pesos dos criterios
     * @param critAlt matriz contendo os criterios e as alternativas
     * @param cBenefCusto matriz contendo os criterios beneficio e custo
     * @param matrizCriterios matriz contendo o valor dos criterios
     * @param matrizNormalizada matriz dos criterios normalizada
     * @param matrizPesada matriz dos criterios pesada
     * @param solucaoIdeal vetor contendo a solucao ideal positiva
     * @param solucaoIdealNeg vetor contendo a solucao ideal negativa
     * @param separacaoIdeal vetor contendo a separacao ideal positiva
     * @param separacaoIdealNeg vetor contendo a separacao ideal negativa
     * @param ci vetor contendo a distancia relativa da solucao ideal positiva e
     * negativa
     * @param melhorEscolha a solucao do problema
     * @param writer objeto Formatter para escrita no ficheiro
     * @throws FileNotFoundException se o ficheiro nao existir
     */
    public static void escrever(double[] vetorPesos, String[][] critAlt,
            String[][] cBenefCusto, double[][] matrizCriterios,
            double[][] matrizNormalizada, double[][] matrizPesada,
            double[] solucaoIdeal, double[] solucaoIdealNeg,
            double[] separacaoIdeal, double[] separacaoIdealNeg, double[] ci,
            String melhorEscolha, Formatter writer) throws FileNotFoundException {

        //imprime para a consola
        prints(vetorPesos, critAlt, cBenefCusto, matrizCriterios, matrizPesada,
                ci, melhorEscolha);

        //escreve para um ficheiro o output
        writer.format("TOPSIS%n%n");
        writer.format("%s%n", "Alternativas");
        writerVetorString(writer, critAlt[1]);
        writer.format("%n%s%n", " ");
        writerVetorDouble(writer, "Vetor Pesos", vetorPesos);
        writer.format("%n%s%n", " ");
        writerMatrizDouble(writer, "Matriz Criterios", matrizCriterios, critAlt, 0);
        writer.format("%n%s%n", " ");
        writerMatrizDouble(writer, "Matriz Normalizada", matrizNormalizada, critAlt, 0);
        writer.format("%n%s%n", " ");
        writerMatrizDouble(writer, "Matriz Pesada", matrizPesada, critAlt, 0);
        writer.format("%n%s%n", " ");
        writer.format("%s%n", "Criterios Beneficio:");
        writerVetorString(writer, cBenefCusto[0]);
        writer.format("%n%s%n", " ");
        writer.format("%s%n", "Criterios Custo:");
        writerVetorString(writer, cBenefCusto[1]);
        writer.format("%n%s%n", " ");
        writerVetorDouble(writer, "Solucao Ideal", solucaoIdeal);
        writer.format("%n%s%n", " ");
        writerVetorDouble(writer, "Solucao Ideal Negativa", solucaoIdealNeg);
        writer.format("%n%s%n", " ");
        writerVetorDouble(writer, "Vetor Proximidades", ci);
        writer.format("%n%s%n", " ");
        writer.format("%n%s: %s", "A alternativa escolhida e", melhorEscolha);
        writer.close();
    }

    /**
     * Modulo que permite escrever no ficheiro um vetor de strings formatado
     *
     * @param write objeto Formatter para escrita no ficheiro
     * @param vetor vetor a imprimir
     */
    private static void writerVetorString(Formatter write, String[] vetor) {
        for (int i = 0; i < vetor.length; i++) {
            if (vetor[i] != null) {
                write.format("%-15.20s", vetor[i]);
            }
        }
    }

    /**
     * Modulo que permite escrever num ficheiro uma matriz de doubles formatada
     *
     * @param write objeto Formatter para escrita no ficheiro
     * @param nome nome da matriz
     * @param matriz matriz a imprimir
     * @param carAlt matriz com os criterios e alternativas
     * @param nLinhaNomes numero da linha a ser lida da matriz carAlt
     */
    private static void writerMatrizDouble(Formatter write, String nome,
            double[][] matriz, String[][] carAlt, int nLinhaNomes) {
        write.format("%s:%n", nome);
        for (int i = 0; i < matriz.length; i++) {
            write.format("%-15s", carAlt[nLinhaNomes][i]);
            for (int j = 0; j < matriz.length; j++) {
                write.format("%-15.2f", matriz[i][j]);
            }
            write.format("%n");
        }
        write.format("%n");
    }

    /**
     * Modulo que permite escrever na consola um vetor de doubles formatada
     *
     * @param write objeto Formatter para escrita no ficheiro
     * @param nome nome do vetor
     * @param vetor vetor a imprimir
     */
    private static void writerVetorDouble(Formatter write, String nome,
            double[] vetor) {
        write.format("%s:%n", nome);
        for (int i = 0; i < vetor.length; i++) {
            write.format("%-15.2f", vetor[i]);
        }
    }

    /**
     * Modulo que permite escrever na consola o resultado do problema
     *
     * @param vetorPesos vetor contendo os pesos dos criterios
     * @param critAlt matriz contendo os criterios e as alternativas
     * @param cBenefCusto matriz contendo os criterios beneficio e custo
     * @param matrizCriterios matriz contendo o valor dos criterios
     * @param matrizPesada matriz dos criterios pesada
     * @param ci vetor contendo a distancia relativa da solucao ideal positiva e
     * negativa
     * @param melhorEscolha a solucao do problema
     */
    public static void prints(double[] vetorPesos, String[][] critAlt,
            String[][] cBenefCusto, double[][] matrizCriterios,
            double[][] matrizPesada, double[] ci, String melhorEscolha) {

        System.out.println("Vetor Pesos");
        printVetorString(critAlt[0]);
        System.out.println();
        printVetorDouble(vetorPesos);

        System.out.println("\n\nCriterios Beneficio: ");
        printVetorString(cBenefCusto[0]);

        System.out.println("\n\nCriterios Custo: ");
        printVetorString(cBenefCusto[1]);

        System.out.println("\n\nAlternativas: ");
        printVetorString(critAlt[1]);

        System.out.println("\n\nMatriz Criterios");
        printMatrizDouble(matrizCriterios);

        System.out.println("\nMatriz Pesada");
        printMatrizDouble(matrizPesada);

        System.out.println("\nVetor Proximidades");
        printVetorDouble(ci);
        System.out.println("\n\nA escolha ideal e: " + melhorEscolha);
    }

    /**
     * Modulo que permite escrever na consola uma matriz de doubles formatada
     *
     * @param matriz matriz a imprimir
     */
    public static void printMatrizDouble(double[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                System.out.printf("%-15.2f", matriz[i][j]);
            }
            System.out.println();
        }
    }

    /**
     * Modulo que permite escrever na consola um vetor de doubles formatado
     *
     * @param vetor vetor a imprimir
     */
    public static void printVetorDouble(double[] vetor) {
        for (int i = 0; i < vetor.length; i++) {
            System.out.printf("%-15.2f", vetor[i]);
        }
    }

    /**
     * Modulo que permite escrever na consola um vetor de strings formatado
     *
     * @param vetor vetor a imprimir
     */
    public static void printVetorString(String[] vetor) {
        for (int i = 0; i < vetor.length; i++) {
            if (vetor[i] != null) {
                System.out.printf("%-15.20s", vetor[i]);
            }
        }
    }

    /**
     * Modulo que vai contar o numero de criterios e de alternativas presentes
     * no ficheiro de entrada
     *
     * @param nomeF
     * @return vetor de 2 posiÃ§Ãµes, na primeira contendo o numero de criterios
     * e na segunda o numero de alternativas
     * @throws FileNotFoundException Caso o ficheiro nÃ£o existe
     */
    public static int[] contarCriteriosAtributos(String nomeF) throws FileNotFoundException {
        int numCriterios = 0;
        int numAlternativas = 0;
        File ficheiro = new File(nomeF);
        if (ficheiro.exists()) {
            Scanner lerF = new Scanner(ficheiro);
            while (lerF.hasNext()) {
                String[] linha = lerF.nextLine().split(" +");
                if (!linha[0].isEmpty() && linha[0].equals("crt")
                        && !isDouble(linha[0])) {
                    numCriterios = linha.length - 1;
                } else {
                    if (!linha[0].isEmpty() && !isDouble(linha[0])
                            && linha[0].equals("alt")) {
                        numAlternativas = linha.length - 1;
                    }
                }
            }
        } else {
            System.out.println("Ficheiro não existente!!");
        }
        int[] critAlt = {numCriterios, numAlternativas};

        return critAlt;
    }

    /**
     * MÃ©todo que vai ler o ficheiro de input e tratar dos valores
     *
     * @param cAlt vetor contendo o numero de criterios e de alternativas
     * @param vetorPesos vetor contendo os pesos (vazio)
     * @param matrizCriterios matriz contendo os criterios
     * @param cBenefCusto matriz contendo na primeira linha os criterios
     * beneficios e na segunda os criterios custo
     * @param critAlt matriz contendo na primeira linha os criterios e na
     * segunda as alternativas
     * @param nomeF nome do ficheiro
     * @return vetor com os pesos
     * @throws FileNotFoundException se o ficheiro nao existe
     */
    public static double[] lerFicheiro(int[] cAlt, double[] vetorPesos,
            double[][] matrizCriterios, String[][] cBenefCusto,
            String[][] critAlt, String nomeF) throws FileNotFoundException {

        int vetor = 0, linhaMatriz = 0, confirmaBenef = 0, confirmaCusto = 0,
                confirma, confirma2, contadorLinha = 0;
        File ficheiro = new File(nomeF);
        String logErrosTOPSISfile = "LogErrosTOPSIS.txt";
        Formatter fileErros = new Formatter(new File(logErrosTOPSISfile));

        if (ficheiro.exists()) { //se o ficheiro existe
            Scanner lerF = new Scanner(ficheiro);
            while (lerF.hasNext()) {
                contadorLinha++;
                confirma = 0;
                confirma2 = 0;
                String[] linha = lerF.nextLine().split(" +");
                for (int j = 0; j < linha.length; j++) {
                    for (int k = 0; k < linha.length; k++) {
                        if (confirma == 0 && confirma2 == 0
                                && isDouble(linha[j]) && !isDouble(linha[k])) {
                            fileErros.format("%s%d%s%n", "Linha ",
                                    contadorLinha, ": Linha deve ter "
                                    + "apenas valores do tipo Double");
                            confirma = 1;
                            break;
                        }
                        if (confirma == 0 && confirma2 == 0
                                && !isDouble(linha[j]) && isDouble(linha[k])) {
                            fileErros.format("%s%d%s%n", "Linha ",
                                    contadorLinha, ": Linha deve ter "
                                    + "apenas valores do tipo String");
                            confirma2 = 1;
                            break;
                        }
                        if (!linha[0].isEmpty() && linha.length == 1) {
                            fileErros.format("%s%d%s%n", "Linha ",
                                    contadorLinha, ": Linha invalida para "
                                    + "o projeto");
                        }
                    }
                }
                if (confirma == 0 && confirma2 == 0) {
                    if (!linha[0].isEmpty() && !isDouble(linha[0])) {
                        //se a linha nao esta vazia nem e double
                        if (linha[0].equals("crt_beneficio")) {
                            for (int i = 1; i < linha.length; i++) {
                                cBenefCusto[0][i - 1] = linha[i];
                            }
                            confirmaBenef = 1; //ja leu os criterios beneficio
                        }
                        if (linha[0].equals("crt_custo")) {
                            for (int i = 1; i < linha.length; i++) {
                                cBenefCusto[1][i - 1] = linha[i];
                            }
                            confirmaCusto = 1; //ja leu os criterios custo
                        }
                        if (linha[0].equals("vec_pesos")) {
                            vetor = 1; //esta na linha dos pesos
                        }
                        if (linha[0].equals("crt")) {
                            for (int i = 1; i < linha.length; i++) {
                                critAlt[0][i - 1] = linha[i];//adiciona os criterios
                            }
                        }
                        if (linha[0].equals("alt")) {
                            for (int i = 1; i < linha.length; i++) {
                                critAlt[1][i - 1] = linha[i];
                                //adiciona as alternativas
                            }
                        }
                    }
                    if (!linha[0].isEmpty() && isDouble(linha[0]) && vetor == 1) {
                        //se linha nao esta vazia, e double esta na linha dos pesos
                        double[] linhaDouble = convertArrayStringParaDouble(linha);
                        vetorPesos = linhaDouble; //insere vetor de pesos
                        //variavel para garantir que nao volta a ler os pesos
                        vetor = 2;
                    } else {
                        if (!linha[0].isEmpty() && isDouble(linha[0])) {
                            //se linha nao esta vazia e e double
                            double[] linhaDouble
                                    = convertArrayStringParaDouble(linha);
                            //insere na matriz
                            matrizCriterios[linhaMatriz] = linhaDouble;
                            linhaMatriz++;//variavel que controla a linha da matriz
                        }
                    }
                }
            }
            if (vetor == 0) {//se nao existir linha de pesos no ficheiro
                double elem = (double) vetorPesos.length;
                double temp = 1 / elem;
                for (int i = 0; i < vetorPesos.length; i++) {
                    //toddos os pesos vao valer o mesmo
                    vetorPesos[i] = temp;
                }
            }
            //se ficheiro nao tem criterios beneficio nem custo
            if (confirmaCusto == 0 && confirmaBenef == 0) {
                //assume todos como beneficio
                cBenefCusto[0] = critAlt[0];
            } else {
                //se ficheiro tem criterios beneficio mas nao tem custo
                if (confirmaCusto == 0 && confirmaBenef == 1) {
                    int controlo, pos = 0;
                    for (int i = 0; i < critAlt[0].length; i++) {
                        controlo = 0;
                        for (int j = 0; j < critAlt[0].length; j++) {
                            //assume automaticamente que os que nao estao
                            //em beneficio sao custo
                            //se criterio for diferente do criterio beneficio
                            if (!critAlt[0][i].equals(cBenefCusto[0][j])) {
                                controlo = 1;
                            } else {
                                controlo = 0;
                                break;
                            }
                        }
                        if (controlo == 1) {
                            //adicionar criterio custo
                            cBenefCusto[1][pos] = critAlt[0][i];
                            pos++;
                        }
                    }
                }
                //se ficheiro tem criterios custo mas nao tem beneficio
                if (confirmaCusto == 1 && confirmaBenef == 0) {
                    int controlo, pos = 0;
                    for (int i = 0; i < critAlt[0].length; i++) {
                        controlo = 0;
                        for (int j = 0; j < critAlt[0].length; j++) {
                            //assume automaticamente que os que nao estao
                            //em custo sao beneficio
                            //se criterio for diferente do criterio custo
                            if (!critAlt[0][i].equals(cBenefCusto[1][j])) {
                                controlo = 1;
                            } else {
                                controlo = 0;
                                break;
                            }
                        }
                        if (controlo == 1) {
                            //adicionar criterio beneficio
                            cBenefCusto[0][pos] = critAlt[0][i];
                            pos++;
                        }
                    }
                }
            }
        } else {
            System.out.println("Ficheiro " + nomeF + " nao existente!");
            fileErros.format("%s%n", "Ficheiro indicado nao existente!");
            System.exit(0);
            fileErros.close();
        }
        fileErros.close();
        return vetorPesos;
    }

    /**
     * Modulo que verifica se uma string pode ser convertida para double
     *
     * @param string String em questao
     * @return valor booleano correspondente ao sucesso da operacao
     */
    public static boolean isDouble(String string) {
        try {
            Double.parseDouble(string);
        } catch (NumberFormatException e) {
            String[] fraccao = string.split("/");
            if (fraccao.length > 1) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * Metodo que serve para converter um array de strings em double
     *
     * @param array array de strings
     * @return array convertido para double
     */
    public static double[] convertArrayStringParaDouble(String[] array) {
        double[] arrayDouble = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            String[] fraccao = array[i].split("/");
            if (fraccao.length == 1) {
                arrayDouble[i] = Double.parseDouble(array[i]);
            } else {
                double elem1 = Double.parseDouble(fraccao[0]);
                double elem2 = Double.parseDouble(fraccao[1]);
                arrayDouble[i] = elem1 / elem2;
            }
        }
        return arrayDouble;
    }

    /**
     * Modulo que vai receber a matriz de criterios e vai fazer o quadrado de
     * cada um dos seus elementos
     *
     * @param matrizCriterios matriz dos criterios
     * @param cAlt vetor com o numero de criterios e de alternativas
     * @return matriz resultado
     */
    public static double[][] matrizCriteriosAoQuadrado(double[][] matrizCriterios,
            int[] cAlt) {

        double[][] matrizCriteriosAoQuadrado = new double[cAlt[0]][cAlt[1]];
        for (int i = 0; i < matrizCriterios.length; i++) {
            for (int j = 0; j < matrizCriterios[i].length; j++) {
                matrizCriteriosAoQuadrado[i][j]
                        = Math.pow(matrizCriterios[i][j], 2);
            }
        }
        return matrizCriteriosAoQuadrado;
    }

    /**
     * Modulo que vai fazer a raiz quadrada da soma das colunas de uma matriz
     *
     * @param matrizCriteriosAoQuadrado matriz em questao
     * @param cAlt vetor com o numero de criterios e de alternativas
     * @return vetor contendo em cada posicao o resultado correspondente a cada
     * coluna
     */
    public static double[] raizSomaColuna(double[][] matrizCriteriosAoQuadrado,
            int[] cAlt) {
        double[] raizSomaColuna = new double[cAlt[1]];
        int sum = 0;
        for (int i = 0; i < matrizCriteriosAoQuadrado.length; i++) {
            for (int j = 0; j < matrizCriteriosAoQuadrado[i].length; j++) {
                sum += matrizCriteriosAoQuadrado[i][j];
                raizSomaColuna[j] += matrizCriteriosAoQuadrado[i][j];
            }
        }
        for (int p = 0; p < raizSomaColuna.length; p++) {
            raizSomaColuna[p] = Math.sqrt(raizSomaColuna[p]);
        }
        return raizSomaColuna;
    }

    /**
     * Modulo que vai normalizar uma matriz de acordo com o metodo TOPSIS
     *
     * @param matrizCriterios matriz com os valores dos criterios do problema
     * @param cAlt vetor com o numero de criterios e de alternativas
     * @param raizSomaColuna vetor contendo o valor da raiz quadrada da soma de
     * cada coluna da matriz criterios com os elementos ao quadrado
     * @return matriz dos criterios normalizada
     */
    public static double[][] normalizaTOPSIS(double[][] matrizCriterios,
            int[] cAlt, double[] raizSomaColuna) {

        double[][] matrizNormalizada
                = new double[matrizCriterios.length][matrizCriterios.length];

        for (int i = 0; i < matrizCriterios.length; i++) {
            for (int j = 0; j < matrizCriterios[i].length; j++) {
                matrizNormalizada[i][j] = matrizCriterios[i][j]
                        / raizSomaColuna[j];
            }
        }
        return matrizNormalizada;
    }

    /**
     * Modulo que vai calcular a matriz de criterios pesada
     *
     * @param matrizCriterios matriz contendo o valor dos criterios
     * @param vetorPesos vetor contendo o peso de cada criterio
     * @return matriz pesada
     */
    public static double[][] matrizPesada(double[][] matrizCriterios,
            double[] vetorPesos) {

        double[][] matrizPesada
                = new double[matrizCriterios.length][matrizCriterios.length];

        for (int i = 0; i < matrizCriterios.length; i++) {
            for (int j = 0; j < matrizCriterios[i].length; j++) {
                matrizPesada[i][j] = matrizCriterios[i][j] * vetorPesos[j];
            }
        }
        return matrizPesada;
    }

    /**
     * Modulo que permite calcular a solucao ideal positiva e negativa para uma
     * matriz
     *
     * @param matrizCriterios matriz contendo o valor dos criterios
     * @param cAlt vetor com o numero de criterios e de alternativas
     * @param critBenefCusto vetor identificando com um valor binario quais os
     * criterios beneficio (1) e os custo (0)
     * @param beneficio valor que fica 0 se estiver a calcular a solucao ideal
     * positiva e 1 se calcular a negativa
     * @param custo valor que fica 1 se estiver a calcular a solucao ideal
     * positiva e 0 se calcular a negativa
     * @return vetor contendo a solucao ideal positiva ou negativa, dependendo
     * da situacao
     */
    public static double[] solucaoIdeal(double[][] matrizCriterios,
            int[] cAlt, int[] critBenefCusto,
            int beneficio, int custo) {

        double solucao[] = new double[cAlt[0]];
        double menor;
        double maior;
        for (int i = 0; i < critBenefCusto.length; i++) {
            menor = Double.MAX_VALUE;
            maior = Double.MIN_VALUE;
            for (int j = 0; j < matrizCriterios[i].length; j++) {
                if (critBenefCusto[i] == beneficio) {
                    if (matrizCriterios[j][i] < menor) {
                        menor = matrizCriterios[j][i];
                        solucao[i] = menor;
                    }
                }
                if (critBenefCusto[i] == custo) {
                    if (matrizCriterios[j][i] > maior) {
                        maior = matrizCriterios[j][i];
                        solucao[i] = maior;
                    }
                }

            }
        }
        return solucao;
    }

    /**
     * Modulo que cria um vetor de inteiros onde cada posicao corresponde a um
     * criterio e tera valor 1 se for criterio beneficio e 0 se for custo
     *
     * @param critAlt matriz contendo os criterios e alternativas
     * @param matrizBenCus matriz contendo criterios beneficio e custo
     * @return vetor de inteiros em questao
     */
    public static int[] criteriosBinario(String[][] critAlt,
            String[][] matrizBenCus) {
        int[] vetor = new int[critAlt[0].length];
        for (int i = 0; i < vetor.length; i++) {
            for (int j = 0; j < vetor.length; j++) {
                if (matrizBenCus[0][i] != null) {
                    //se criterio beneficio e igual ao criterio em j
                    if (matrizBenCus[0][i].equals(critAlt[0][j])) {
                        vetor[j] = 1;
                    }
                }
            }
        }
        return vetor;
    }

    /**
     * Modulo que calcula a separacao ideal positiva e negativa
     *
     * @param matrizCriterios matriz contendo os criterios
     * @param solucaoIdeal vetor contendo a solucao ideal positiva ou negativa
     * @param cAlt vetor contendo o numero de criterios e alternativas
     * @return vetor com a separacao ideal positiva ou negativa, dependendo da
     * situacao
     */
    public static double[] separacaoIdeal(double[][] matrizCriterios,
            double[] solucaoIdeal, int[] cAlt) {

        // valores antes da soma
        double[][] preSoma = new double[cAlt[0]][cAlt[1]];
        double[] soma = new double[preSoma.length];
        double[] si = new double[soma.length];
        for (int i = 0; i < matrizCriterios.length; i++) {
            for (int j = 0; j < matrizCriterios[i].length; j++) {
                preSoma[j][i] = Math.pow(matrizCriterios[j][i]
                        - solucaoIdeal[i], 2);
            }
        }
        for (int i = 0; i < preSoma.length; i++) {
            for (int j = 0; j < preSoma[i].length; j++) {
                soma[i] = preSoma[i][j] + soma[i];
            }
        }
        for (int i = 0; i < soma.length; i++) {
            si[i] = Math.sqrt(soma[i]);
        }

        return si;
    }

    /**
     * Metodo que calcula a distancia relativa da solucao ideal positiva e
     * negativa (CI)
     *
     * @param separacaoIdealNeg vetor contendo a separacao ideal negativa
     * @param separacaoIdeal vetor contendo a separacao ideal positiva
     * @return vetor com a distancia relativa
     */
    public static double[] ci(double[] separacaoIdealNeg,
            double[] separacaoIdeal) {

        double[] ci = new double[separacaoIdeal.length];
        for (int i = 0; i < separacaoIdeal.length; i++) {
            ci[i] = separacaoIdealNeg[i] / (separacaoIdealNeg[i]
                    + separacaoIdeal[i]);
        }

        return ci;
    }

    /**
     * Modulo que permite retornar o resultado do programa
     *
     * @param ci vetor contendo a distancia relativa da solucao ideal
     * @param critAlt matriz contendo os criterios e as alternativas
     * @return a melhor alternativa do problema proposto
     */
    public static String melhorEscolha(double[] ci, String[][] critAlt) {
        String melhorEscolha;
        double maior = 0;
        int contador = 0;
        maior = ci[0];
        for (int i = 0; i < ci.length; i++) {
            if (ci[i] > maior) {
                maior = ci[i];
            }
        }
        for (int j = 0; j < ci.length; j++) {
            contador++;
            if (ci[j] == maior) {
                break;
            }
        }
        melhorEscolha = critAlt[1][contador - 1];
        return melhorEscolha;
    }
}
