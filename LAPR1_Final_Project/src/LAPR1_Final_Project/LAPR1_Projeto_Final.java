/**
 * Projeto no ambito de LAPR1 com o objetivo de implementar um algoritmo em que
 * resolva problemas de Apoio a Decisao Multicriterio (MADMC)
 * Neste projeto sao incorporados os metodos AHP (Analytical Hierarchy Process)
 * e TOPSIS (Technique of Order Preference by Similarity to Ideal Solution)
 */
package LAPR1_Final_Project;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

/**
 *
 * @author Vitor Melo
 * @author Vitor Cardoso
 * @author Filipe Ferreira
 * @author Guilherme Oliveira
 */
public class LAPR1_Projeto_Final {

    /**
     * Metodo com todos os processos referentes ao método aproximado do calculo
     * do metodo AHP
     *
     * @param nomeF nome de ficheiro de leitura
     * @param nomeFE nome de ficheiro de escrita
     * @param limiar limite para remover criterios
     * @param limiarRC limite que nao pode exceder o RC
     * @throws FileNotFoundException
     */
    private static void metodoAproximado(String nomeF, String nomeFE,
            double limiar, double limiarRC) throws FileNotFoundException {

        //guarda o numero de criterio e alternativas
        int[] nCriAlt = Common.contarCriteriosAtributos(nomeF);

        //guarda os nomes dos criterios e alternativas
        String[][] nomesCA;
        if (nCriAlt[0] >= nCriAlt[1]) {
            nomesCA = new String[2][nCriAlt[0]];
        } else {
            nomesCA = new String[2][nCriAlt[1]];
        }

        //guarda os valores dos criterios
        double[][] matrizC;
        //guarda os valores de cada criterio
        double[][][] matrizesA;

        matrizC = new double[nCriAlt[0]][nCriAlt[0]];
        matrizesA = new double[nCriAlt[0]][nCriAlt[1]][nCriAlt[1]];

        if (nCriAlt[0] > 2 && nCriAlt[1] > 2) {
            //le o ficheiro para preencher as matrizes criadas anteriormente
            Common.lerFicheiro(nCriAlt, matrizesA, matrizC, nomesCA, nomeF);

            //verifica se as matrizes estao devidamente preenchidas
            if (reciprocidadeMatrizes(matrizesA, matrizC, nomesCA)) {
                double[][] cMatrizC;     //cópia da matriz dos criterios
                double[][][] cMatrizesA; //copia das matrizes das alternativas
                double[] vetorC; //vetor prioridades dos criterios
                String[] cRemovidos;   // guarda nomes dos criterios removidos
                String[] naoRemovidos; //nomes dos criterios que nao foram removidos

                naoRemovidos = new String[nCriAlt[0]];
                //conta quantos nao nomes removeu
                int nNaoRem = naoRemovidos.length;
                cMatrizesA = new double[nCriAlt[0]][nCriAlt[1]][nCriAlt[1]];
                vetorC = new double[nCriAlt[0]];
                cRemovidos = new String[0];

                //cria copias das matrizes de entrada originais
                cMatrizC = Common.copiaMatriz(matrizC);
                for (int i = 0; i < matrizesA.length; i++) {
                    cMatrizesA[i] = Common.copiaMatriz(matrizesA[i]);
                }

                /*se o limiar for maior ou igual a 0 testa se algum valor do vetor 
            esta abaixo desse limiar*/
                if (limiar >= 0) {
                    double[][] tMatrizC; //matriz temporaria de criterios
                    int index; //indice do menor valor abaixo do limiar

                    System.arraycopy(nomesCA[0], 0, naoRemovidos, 0,
                            naoRemovidos.length);

                    do {
                        tMatrizC = Common.copiaMatriz(cMatrizC);
                        AproxAHP.normalizaMatrizes(tMatrizC);
                        vetorC = AproxAHP.calculaVetorPrioridades(tMatrizC);
                        index = Common.compararLimiarVetor(vetorC, limiar);
                        if (index != -1) {
                            --nNaoRem;
                            for (int i = index; i < naoRemovidos.length - 1; i++) {
                                naoRemovidos[i] = naoRemovidos[i + 1];
                            }
                            cMatrizC
                                    = Common.removerElementos2D(cMatrizC, index);
                            cMatrizesA
                                    = Common.removerElementos3D(cMatrizesA, index);
                        }
                    } while ((index != -1) && (cMatrizC.length > 2));
                }

                if (cMatrizC.length <= 2) {
                    System.out.println("Erro: Com o limiar de "
                            + limiar + " apenas sobraram 2 ou menos criterios!");
                } else {
                    if (limiar >= 0) {
                        /*obtem os nomes dos criterios removidos*/
                        cRemovidos
                                = Common.registarRemovidos(nomesCA, naoRemovidos,
                                        nNaoRem);
                        naoRemovidos = Common.registarNRemovidos(nomesCA,
                                naoRemovidos, nNaoRem);
                    }
                    double[][] vetoresPriorA;
                    /*matriz com vetores prioridades de 
                cada criterio com alternativas*/
                    double[][] vMaxCICR;
                    /*matriz com os valores maximos, 
                o indice de consistencia e razao de consistencia*/

                    vetoresPriorA
                            = new double[cMatrizesA.length][cMatrizesA[0][0].length];
                    vMaxCICR = new double[cMatrizesA.length + 1][3];

                    //normaliza as matrizes
                    AproxAHP.normalizaMatrizes(cMatrizC);
                    for (int i = 0; i < vetorC.length; i++) {
                        AproxAHP.normalizaMatrizes(cMatrizesA[i]);
                    }
                    vetorC = AproxAHP.calculaVetorPrioridades(cMatrizC);

                    AproxAHP.copiaVetoresAlternativas(vetoresPriorA, cMatrizesA);

                    /* Valor máximo, CI e CR para os criterios*/
                    AproxAHP.preencheVMaxCICR(vMaxCICR, matrizC, vetorC, 0);

                    /* Valor máximo, CI e CR para cada matriz de cada criterio*/
                    for (int i = 1; i < vMaxCICR.length; i++) {
                        AproxAHP.preencheVMaxCICR(vMaxCICR, matrizesA[i - 1],
                                vetoresPriorA[i - 1], i);
                    }

                    if (Common.verificarConsistencia(vMaxCICR, limiarRC, nomesCA)) {
                        //matriz transposta dos vetores de cada matriz das alternativas
                        double[][] matrizT
                                = Common.transporMatriz(vetoresPriorA);
                        //vetor prioridades composta
                        double[] vetorComposto
                                = Common.multiplicarMV(matrizT, vetorC);

                        //escrever para ficheiro todos os campos
                        Common.escrever(2, nomeFE, nomesCA, matrizC, matrizesA,
                                vMaxCICR, vetorComposto, vetorC, vetoresPriorA,
                                cMatrizC, cMatrizesA, cRemovidos, naoRemovidos);
                    }
                }
            }
        } else {
            System.out.println("Erro: Menos de 2 criterios ou alternativas!");
        }
    }

    /**
     * Metodo com todos os processos referentes ao método exato do calculo do
     * metodo AHP usando a biblioteca la4j
     *
     * @param nomeFE nome do ficheiro de escrita
     * @param matrizC matriz dos criterios
     * @param matrizA1 matriz do primeiro criterio
     * @param matrizA2 matriz do segundo criterio
     * @param matrizA3 matriz do terceiro criterio
     * @param nomesCA matriz do nome dos criterios e das alternativas
     * @throws FileNotFoundException
     */
    private static void metodoExato(String nomeF, String nomeFE, double limiar,
            double limiarRC) throws FileNotFoundException {

        int[] nCriAlt = Common.contarCriteriosAtributos(nomeF);
        String[][] nomesCA; // guarda nomes dos criterios e alternativas
        if (nCriAlt[0] >= nCriAlt[1]) {
            nomesCA = new String[2][nCriAlt[0]];
        } else {
            nomesCA = new String[2][nCriAlt[1]];
        }
        double[][] matrizC; //matriz dos criterios
        double[][][] matrizesA; //matrizes de cada criterio com alternativas
        double[][] vetoresValoresA; //vetores e valores de cada matriz criterio

        matrizC = new double[nCriAlt[0]][nCriAlt[0]];
        matrizesA = new double[nCriAlt[0]][nCriAlt[1]][nCriAlt[1]];
        vetoresValoresA = new double[nCriAlt[0]][nCriAlt[1] + 1];

        if (nCriAlt[0] > 2 && nCriAlt[1] > 2) {
            Common.lerFicheiro(nCriAlt, matrizesA, matrizC, nomesCA, nomeF);

            if (reciprocidadeMatrizes(matrizesA, matrizC, nomesCA)) {
                double[] vetorValorC; //valor proprio e vetor proprio dos criterios
                double[] vetorC; //vetor proprio dos criterios retirado do vetorvalorC
                String[] cRemovidos;// guarda nomes dos criterios removidos
                String[] naoRemovidos; //guarda os nomes que nao foram removidos

                naoRemovidos = new String[nCriAlt[0]];
                cRemovidos = new String[0];
                vetorValorC = new double[nCriAlt[0]];
                int nRemovidos = naoRemovidos.length; //conta quantos nomes nao removeu


                /*cria copias de todas as matrizes*/
                double[][] cMatrizC = Common.copiaMatriz(matrizC);
                double[][][] cMatrizesA
                        = new double[nCriAlt[0]][nCriAlt[1]][nCriAlt[1]];
                for (int i = 0; i < cMatrizesA.length; i++) {
                    cMatrizesA[i] = Common.copiaMatriz(matrizesA[i]);
                }

                if (limiar >= 0) {
                    int index;
                    System.arraycopy(nomesCA[0], 0, naoRemovidos, 0,
                            naoRemovidos.length);

                    do {
                        vetorValorC
                                = ExactAHP.calculoValorEVetorProprio(cMatrizC);
                        vetorC = ExactAHP.retirarVetorProprio(vetorValorC);
                        index = Common.compararLimiarVetor(vetorC, limiar);
                        if (index != -1) {
                            --nRemovidos;
                            for (int i = index; i < naoRemovidos.length - 1; i++) {
                                naoRemovidos[i] = naoRemovidos[i + 1];
                            }
                            cMatrizC
                                    = Common.removerElementos2D(cMatrizC, index);
                            cMatrizesA
                                    = Common.removerElementos3D(cMatrizesA, index);
                        }
                    } while ((index != -1) && (cMatrizC.length > 2));
                }

                if (cMatrizC.length <= 2) {
                    System.out.println("Erro: Com o limiar de "
                            + limiar + " apenas sobraram 2 ou menos criterios!");
                } else {
                    if (limiar >= 0) {
                        cRemovidos
                                = Common.registarRemovidos(nomesCA, naoRemovidos,
                                        nRemovidos);
                        naoRemovidos = Common.registarNRemovidos(nomesCA,
                                naoRemovidos, nRemovidos);
                    }
                    for (int i = -1; i < matrizesA.length; i++) {
                        if (i == -1) {
                            vetorValorC
                                    = ExactAHP.calculoValorEVetorProprio(cMatrizC);
                        } else {
                            vetoresValoresA[i]
                                    = ExactAHP.calculoValorEVetorProprio(cMatrizesA[i]);
                        }
                    }
                    double[][] vMaxCICR = new double[nCriAlt[0] + 1][3];
                    vMaxCICR[0]
                            = ExactAHP.preencheVMaxCICR(vetorValorC, cMatrizC);

                    for (int i = 1; i < vMaxCICR.length; i++) {
                        vMaxCICR[i]
                                = ExactAHP.preencheVMaxCICR(
                                        vetoresValoresA[i - 1],
                                        cMatrizesA[i - 1]);
                    }

                    if (Common.verificarConsistencia(vMaxCICR, limiarRC,
                            nomesCA)) {
                        double[] vetorProprioC
                                = ExactAHP.retirarVetorProprio(vetorValorC);
                        double[][] vetoresProprios
                                = new double[nCriAlt[0]][nCriAlt[1]];
                        for (int i = 0; i < vetoresProprios.length; i++) {
                            vetoresProprios[i]
                                    = ExactAHP.retirarVetorProprio(vetoresValoresA[i]);
                        }
                        double[][] vetoresPropriosT
                                = Common.transporMatriz(vetoresProprios);
                        double[] vetorComposto
                                = Common.multiplicarMV(vetoresPropriosT,
                                        vetorProprioC);
                        Common.escrever(1, nomeFE, nomesCA, cMatrizC, cMatrizesA,
                                vMaxCICR, vetorComposto, vetorProprioC,
                                vetoresProprios, cRemovidos, naoRemovidos);

                    }
                }
            }
        } else {
            System.out.println("Erro: Menos de 2 criterios ou alternativas!");
        }
    }

    /**
     * Metodo que executa todos os calculos relativos ao metodo TOPSIS
     *
     * @param nomeFE nome de ficheiro de escrita
     */
    private static void metodoTOPSIS(String nomeF, String nomeFE)
            throws FileNotFoundException {
        Formatter writer = new Formatter(new File(nomeFE));
        int[] cAlt = Topsis.contarCriteriosAtributos(nomeF);
        String[][] critAlt;
        String[][] cBenefCusto;
        if (cAlt[0] >= cAlt[1]) { //se houver mais criterios que alternativas
            critAlt = new String[2][cAlt[0]];
            cBenefCusto = new String[2][cAlt[0]];
        } else { //se houver mais alternativas que criterios
            critAlt = new String[2][cAlt[1]];
            cBenefCusto = new String[2][cAlt[1]];
        }
        double[][] matrizCriterios = new double[cAlt[0]][cAlt[1]];
        double[] vetorPesos = new double[cAlt[0]];

        vetorPesos
                = Topsis.lerFicheiro(cAlt, vetorPesos, matrizCriterios,
                        cBenefCusto, critAlt, nomeF); //le o ficheiro

        int[] critBenefCusto = Topsis.criteriosBinario(critAlt, cBenefCusto);

        //faz o quadrado de cada elemento da matriz
        double[][] matrizCriteriosAoQuadrado
                = Topsis.matrizCriteriosAoQuadrado(matrizCriterios, cAlt);

        //faz raiz da soma das colunas da matriz resultante acima
        double[] raizSomaColuna
                = Topsis.raizSomaColuna(matrizCriteriosAoQuadrado, cAlt);

        //normaliza matriz
        double[][] matrizNormalizada
                = Topsis.normalizaTOPSIS(matrizCriterios, cAlt, raizSomaColuna);

        //Matriz com pesos
        double[][] matrizPesada
                = Topsis.matrizPesada(matrizNormalizada, vetorPesos);

        // Solucao ideal         
        double[] solucaoIdeal
                = Topsis.solucaoIdeal(matrizPesada, cAlt, critBenefCusto, 0, 1);

        //Solucao ideal negativa
        double[] solucaoIdealNeg
                = Topsis.solucaoIdeal(matrizPesada, cAlt, critBenefCusto, 1, 0);

        //Separacao ideal
        double[] separacaoIdeal
                = Topsis.separacaoIdeal(matrizPesada, solucaoIdeal, cAlt);

        //Separacao Ideal negativa
        double[] separacaoIdealNeg
                = Topsis.separacaoIdeal(matrizPesada, solucaoIdealNeg, cAlt);

        // Cálculo do Ci (distancia relativa da solucao ideal)
        double[] ci = Topsis.ci(separacaoIdealNeg, separacaoIdeal);

        //escolher alternativa
        String melhorEscolha = Topsis.melhorEscolha(ci, critAlt);

        //escrever em ficheiro
        Topsis.escrever(vetorPesos, critAlt, cBenefCusto, matrizCriterios,
                matrizNormalizada, matrizPesada, solucaoIdeal, solucaoIdealNeg,
                separacaoIdeal, separacaoIdealNeg, ci, melhorEscolha, writer);
    }

    /**
     * Menu apresentado no ecra para escolha do metodo a ser usado para os
     * calculos
     *
     * @param nomeFE nome do ficheiro de escrita
     * @param nomeF nome do ficheiro de leitura
     * @throws FileNotFoundException
     */
    private static void menu(String nomeF, String nomeFE, double limiar,
            double limiarRC)
            throws FileNotFoundException {
        Scanner scan = new Scanner(System.in);
        System.out.printf("%n               AHP%n"
                + "==================================%n"
                + "|  Introduza o método desejado:  |%n"
                + "==================================%n"
                + "|          1 - Exato             |%n"
                + "|          2 - Aproximado        |%n"
                + "|          3 - Cancelar          |%n"
                + "==================================%n%n"
                + "Escolha a sua opção:  ");
        switch (scan.next()) {
            case "1":
                metodoExato(nomeF, nomeFE, limiar, limiarRC);
                break;
            case "2":
                metodoAproximado(nomeF, nomeFE, limiar, limiarRC);
                break;
            case "3":
                break;
            default:
                System.out.println("Erro: Input nao reconhecido");
                menu(nomeF, nomeFE, limiar, limiarRC);
                break;
        }
    }

    /**
     * Metodo dependente do metodo verificaReciprocidade para verificar se as
     * matrizes estao com erros de preemchimento
     *
     * @param matrizesA matrizes alternativas
     * @param matrizC matriz criterios
     * @param nomesCA nomes das matrizes e das alternativas
     * @return true se as matrizes testadas estao bem preenchidas, false se o
     * contrário
     */
    private static boolean reciprocidadeMatrizes(double[][][] matrizesA,
            double[][] matrizC, String[][] nomesCA) {
        //verifica as matrizes das alternativas
        for (int i = 0; i < matrizesA.length; i++) {
            if (!verificaReciprocidade(matrizesA[i], nomesCA[0][i])) {
                return false;
            }
        }
        //verifica a reciprocidade da matriz criterios
        if (!verificaReciprocidade(matrizC, "Criterios")) {
            return false;
        }
        return true;
    }

    /**
     * Metodo para verificar se numa matriz há reciprocidade para evitar erros
     * de calculo possiveis
     *
     * @param matriz matriz a verificar a reciprocidade
     * @param nome nome da matriz a ser testada
     * @return true se a matriz estiver bem preenchida, false se o contrário
     */
    private static boolean verificaReciprocidade(double[][] matriz,
            String nome) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = i; j < matriz[i].length; j++) {
                if (matriz[i][j] * matriz[j][i] != 1.0) {
                    System.out.printf("Erro no elemento [%d][%d] "
                            + "da matriz %s.%n", j + 1, i + 1, nome);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Metodo que mostra uma mensagem de ajuda com os possiveis comandos e
     * execucao do programa
     */
    private static void showHelp() {
        System.out.printf("%nUtilizacao: java -jar NomePrograma.jar -M [NUM] -L"
                + " [NUM] -S [NUM] input.txt output.txt%n%n -M [1|2], escolha "
                + "do metodo a ser usado (1 AHP - 2 TOPSIS)%n Se -M 2 a opcao"
                + " -L e -S se introduzidas serao ignoradas%n%n -L [>= 0] "
                + "escolha do limiar a ser usado no método AHP%n Se -L nao "
                + "for invocado o limiar sera por default 0%n%n -S [>= 0.1] "
                + "escolha do limiar para a Razao de Consistencia (RC)%n");
    }

    /**
     * Metodo que interpreta os comandos introduzidos na linha de comandos e
     * verifica se ha erros e se e possivel correr a aplicacao, caso falte
     * campos apresenta uma mensagem de erro e termina a aplicacao nao fazendo
     * mais nenhum output
     *
     * @param args
     * @throws FileNotFoundException
     */
    private static void parser(String[] args) throws FileNotFoundException {
        if (args.length > 0) {
            boolean foundM = false; //controla se encontrou um -m
            int opMet = 0; //metodo pretendido
            double limiar = -1; //limiar do criterio a ser removido
            double limiarRC = 0.1; //limite do RC por default

            //verifica opcoes
            for (int i = 0; i < args.length; i++) {
                if (i == 0 || i == 2 || i == 4) {
                    switch (args[i].toLowerCase()) {
                        case "-m":
                            try {
                                opMet = Integer.parseInt(args[i + 1]);
                                foundM = true;
                                if (opMet < 1 || opMet > 2) {
                                    System.out.println("Metodo nao existente!");
                                    System.exit(0);
                                }
                            } catch (NumberFormatException e) {
                                System.out.println("Metodo escolhido "
                                        + "invalido!");
                                System.exit(0);
                            }
                            break;

                        case "-l":
                            try {
                                limiar = Double.parseDouble(args[i + 1]);
                                if (limiar < 0) {
                                    System.out.println("Limiar nao pode ser "
                                            + "menor que 0!");
                                    System.exit(0);
                                }
                            } catch (NumberFormatException e) {
                                System.out.println("Limiar " + args[i + 1]
                                        + " invalido!");
                                System.exit(0);
                            }
                            break;

                        case "-s":
                            try {
                                limiarRC = Double.parseDouble(args[i + 1]);
                                if (limiarRC < 0.1) {
                                    System.out.printf("Limiar nao pode ser "
                                            + "menor que 0.1!%nValor inserido: "
                                            + "%s%n", args[i + 1]);
                                    System.exit(0);
                                }
                            } catch (NumberFormatException e) {
                                System.out.println("Limiar " + args[i + 1]
                                        + " invalido!");
                                System.exit(0);
                            }
                            break;

                        default:
                            if (i < args.length - 2) {
                                System.out.println("Opcao " + args[i]
                                        + " invalida!");
                                System.exit(0);
                                break;
                            }
                    }
                }
                if (i == args.length - 1 || i == args.length - 2) {
                    if (!args[i].contains(".txt")) {
                        System.out.println("Nomes de ficheiro invalidos ou "
                                + "em falta!");
                        System.exit(0);
                    }
                }
            }
            if (foundM && (opMet == 1 || opMet == 2)) {
                if (opMet == 1) {
                    String nomeF = args[args.length - 2];
                    String nomeFE = args[args.length - 1];
                    //inicia o calculo do AHP ao chamar o menu
                    menu(nomeF, nomeFE, limiar, limiarRC);
                }
                if (opMet == 2) {
                    String nomeF = args[args.length - 2];
                    String nomeFE = args[args.length - 1];
                    metodoTOPSIS(nomeF, nomeFE);
                }
            } else {
                System.out.println("Opcao -M nao encontrada");
                showHelp();
            }
        } else {
            System.out.println("Sem parametros introduzidos!");
            showHelp();
        }
    }

    /**
     * Metodo que inicia a aplicacao Inicia o parser para interpretar os
     * comandos inseridos pelo utilizador
     *
     * @param args
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        parser(args);
    }
}
