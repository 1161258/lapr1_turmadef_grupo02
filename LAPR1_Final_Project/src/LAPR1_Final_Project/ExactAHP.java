package LAPR1_Final_Project;

import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;

/**
 *
 * @author Vitor Melo
 * @author Vitor Cardoso
 * @author Filipe Ferreira
 * @author Guilherme Oliveira
 */
public class ExactAHP {

    /**
     * Método usado para calcular o valor e o vetor próprio de uma matriz
     *
     * @param matriz matriz em questão
     * @return vetor contendo o valor próprio na primeira posição e o vetor
     * próprio nas restantes
     */
    public static double[] calculoValorEVetorProprio(double[][] matriz) {
        Matrix matrizCriterios = new Basic2DMatrix(matriz);
        EigenDecompositor eigenD = new EigenDecompositor(matrizCriterios);
        Matrix[] mattD = eigenD.decompose();
        double[][] matrizVetorProprio = converterMatrixParaDouble(mattD[0]);
        double[][] matrizValorProprio = converterMatrixParaDouble(mattD[1]);
        double[] valorProprio = getValorProprioEPos(matrizValorProprio);
        double[] vetorProprio 
                = getVetorProprio(matrizVetorProprio, valorProprio[1]);
        double[] valorMaisVetor = new double[matrizVetorProprio.length + 1];
        valorMaisVetor[0] = valorProprio[0];
        for (int i = 1; i < valorMaisVetor.length; i++) {
            valorMaisVetor[i] = vetorProprio[i - 1];
        }
        return valorMaisVetor;
    }

    /**
     * Método usado para converter uma matriz do tipo Matrix para o tipo
     * double[][]
     *
     * @param matriz matriz do tipo Matrix
     * @return matriz do tipo double[][]
     */
    public static double[][] converterMatrixParaDouble(Matrix matriz) {
        double[][] matrizDouble = matriz.toDenseMatrix().toArray();
        return matrizDouble;
    }

    /**
     * Método usado para obter o valor próprio e a sua coluna de uma matriz dada
     * pelo "EigenDecompositor.decompose()", já convertida para double[][]
     *
     * @param matriz matriz em questão
     * @return vetor contendo o valor próprio na primeira posição e a sua coluna
     * na segunda
     */
    public static double[] getValorProprioEPos(double[][] matriz) {
        double[] max = {Double.MIN_VALUE, 0};
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                if (matriz[i][j] > max[0]) {
                    max[0] = matriz[i][j];
                    max[1] = j;
                }
            }
        }
        return max;
    }

    /**
     * Método usado para retirar o vetor próprio de uma matriz dada pelo
     * "EigenDecompositor.decompose()", já convertida para double[][]
     *
     * @param matriz matriz em questão
     * @param coluna coluna da matriz onde o vetor próprio se encontra
     * @return vetor próprio
     */
    public static double[] getVetorProprio(double[][] matriz, double coluna) {
        Double d = coluna;
        int colunaInt = d.intValue();
        double[] vetorProprio = new double[matriz.length];
        for (int i = 0; i < matriz.length; i++) {
            vetorProprio[i] = matriz[i][colunaInt];
        }
        //trataVetorProprio(vetorProprio);
        normalizaVetor(vetorProprio); //normaliza o vetor
        return vetorProprio;
    }

    /**
     * Método usado para tornar positivos os valores de um vetor
     *
     * @param vetor vetor em questão
     * @return vetor com os valores positivos
     */
    public static double[] trataVetorProprio(double[] vetor) {
        if (vetor[0] < 0) {
            for (int i = 0; i < vetor.length; i++) {
                vetor[i] *= -1;

            }
        }
        return vetor;
    }

    /**
     * Metodo que normaliza o vetor proprio calculado
     *
     * @param vetor vetor a normalizar
     */
    public static void normalizaVetor(double[] vetor) {
        double soma = 0;
        for (int i = 0; i < vetor.length; i++) {
            soma += vetor[i];
        }
        for (int i = 0; i < vetor.length; i++) {
            vetor[i] /= soma;
        }
    }

    /**
     * Método usado para retirar o vetor proprio do vetor contendo o valor e o
     * vetor proprio de uma matriz
     *
     * @param vetor valor e o vetor proprio de uma matriz
     * @return vetor proprio de uma matriz
     */
    public static double[] retirarVetorProprio(double[] vetor) {
        double[] temp = new double[vetor.length - 1];
        for (int i = 1; i < vetor.length; i++) {
            temp[i - 1] = vetor[i];
        }
        return temp;
    }

    /**
     * Método usado para agrupar os vetores proprios das matrizes em apenas uma
     * só matriz
     *
     * @param matrizValorAlternativas matriz onde serão adicionados os vetors
     * proprios
     * @param vetorProprioEstilo vetor proprio de uma matriz
     * @param vetorProprioConfiabilidade vetor proprio de uma matriz
     * @param vetorProprioConsumo vetor proprio de uma matriz
     * @return matriz composta por os vetores proprios
     */
    public static double[][] juntarVetorEmMatriz(
            double[][] matrizValorAlternativas, double[] vetorProprioEstilo,
            double[] vetorProprioConfiabilidade, double[] vetorProprioConsumo) {

        matrizValorAlternativas[0] = vetorProprioEstilo;
        matrizValorAlternativas[1] = vetorProprioConfiabilidade;
        matrizValorAlternativas[2] = vetorProprioConsumo;
        return matrizValorAlternativas;
    }

    /**
     * Metodo para preencher a matriz vMaxCICR com os valores proprios maximos,
     * o indice de consistencia e a razao de consistencia através dos vetores
     * obtidos usando a biblioteca la4j
     *
     * @param matriz matriz de entrada nao normalizada
     * @param vetor vetor prioridades relativo a matriz nao normalizada
     * @return vetor com o valor proprio, o IC e RC
     */
    public static double[] preencheVMaxCICR(double[] vetor,
            double[][] matriz) {

        double[] vMaxCICR = new double[3];
        vMaxCICR[0] = vetor[0];
        vMaxCICR[1] = Common.calculaCI(vMaxCICR[0], matriz.length);
        vMaxCICR[2] = Common.calculaCR(vMaxCICR[1], matriz.length);
        return vMaxCICR;
    }

}
