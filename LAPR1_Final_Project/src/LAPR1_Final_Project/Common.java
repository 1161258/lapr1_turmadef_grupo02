package LAPR1_Final_Project;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

/**
 *
 * @author Vitor Melo
 * @author Vitor Cardoso
 * @author Filipe Ferreira
 * @author Guilherme Oliveira
 */
public class Common {

    /**
     * Matriz RI (Random Consistency Index)
     */
    private static final double[] RI = {
        0.00, 0.00, 0.58, 0.90, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49};

    /**
     * Calcula o Indice de Consistencia de uma matriz CI (Consistency Index)
     *
     * @param vMax valor proprio maximo de uma matriz
     * @param ordem ordem da matriz determinada
     * @return
     *
     * ((valor proprio maximo - ordem da matriz) / (ordem da matriz - 1))
     */
    public static double calculaCI(double vMax, int ordem) {
        return (vMax - ordem) / (ordem - 1);
    }

    /**
     * Calcula a Razao de Consistencia de uma matriz CR (Consistency Ratio)
     *
     * @param CI Indice de Consistencia calculado
     * @param ordem Ordem da matriz associada ao IC calculado
     * @return (IC / RI) Em que RI e a tabela de Random Consistency Index
     */
    public static double calculaCR(double CI, int ordem) {
        return (CI / RI[ordem - 1]);
    }

    /**
     * Copia os elementos de uma matriz para outra matriz de destino
     *
     * @param origem matriz de origem
     * @return matriz de destino igual a de origem
     */
    public static double[][] copiaMatriz(double[][] origem) {
        double[][] destino = new double[origem.length][origem[0].length];
        for (int i = 0; i < origem.length; i++) {
            for (int j = 0; j < origem[0].length; j++) {
                destino[i][j] = origem[i][j];
            }
        }
        return destino;
    }

    /**
     * Método usado para multiplicar a matriz contendo os vetores de
     * alternativas pelo vetor proprio da matriz dos critérios
     *
     * @param matriz matriz que contém os vetores de alternativas
     * @param vetor vetor proprio da matriz dos critérios
     * @return vetor resultado da multiplicação
     */
    public static double[] multiplicarMV(double[][] matriz,
            double[] vetor) {

        double soma;
        double[] matRes = new double[matriz.length];
        if (matriz[0].length == vetor.length) {
            for (int i = 0; i < matriz.length; i++) {
                soma = 0;
                for (int j = 0; j < vetor.length; j++) {
                    soma += (matriz[i][j] * vetor[j]);
                }
                matRes[i] = soma;
            }
        }
        return matRes;
    }

    /**
     * Metodo usado para fazer a matriz transposta de uma matriz
     *
     * @param matriz matriz em questão
     * @return matriz transposta da matriz em questao
     */
    public static double[][] transporMatriz(double[][] matriz) {
        double[][] matrizT = new double[matriz[0].length][matriz.length];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                matrizT[j][i] = matriz[i][j];
            }
        }
        return matrizT;
    }

    /**
     * Compara valor do limiar com os valores do vetor prioridades dos criterios
     * e retorna o indice em que o valor e mais baixo
     *
     * @param vetor vetor de prioridades a ser comparado
     * @param limiar limiar para o qual o vetor tem de ter prioridades
     * inferiores
     * @return indice do menor valor do vetor prioridades
     */
    public static int compararLimiarVetor(double[] vetor, double limiar) {
        int nIndexes = -1;
        int[] indexes = new int[vetor.length];
        //obtem os indices abaixo do limiar
        for (int i = 0; i < vetor.length; i++) {
            if (vetor[i] <= limiar) {
                indexes[++nIndexes] = i;
            }
        }
        if (nIndexes >= 0) {
            //retorna o indice do vetor com o menor valor
            int menorIndex = indexes[0];
            for (int i = 0; i < nIndexes; i++) {
                if (vetor[indexes[i]] < vetor[menorIndex]) {
                    menorIndex = i;
                }
            }
            return menorIndex;
        }
        return -1;
    }

    /**
     * Remove elementos da matriz na linha e coluna
     *
     * @param matriz matriz para remover os elementos da linha e coluna
     * @param indice indice da linha e coluna a remover
     * @return matriz com linha e coluna eliminados
     */
    public static double[][] removerElementos2D(double[][] matriz, int indice) {
        double[][] novaMatriz
                = new double[matriz.length - 1][matriz.length - 1];
        int x = 0;
        int y = 0;
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (i != indice && j != indice) {
                    novaMatriz[x][y] = matriz[i][j];
                    y++;
                    if (y == novaMatriz.length) {
                        x++;
                        y = 0;

                    }
                }
            }
        }
        return novaMatriz;
    }

    /**
     * Metodo que regista os nomes que nao foram removidos para preparar o
     * output
     *
     * @param nomesCA matriz dos nomes dos criterios e alternativas
     * @param noNRem vetor com nomes nao removidos com contador
     * @param nNaoRem numero de nao removidos
     * @return vetor com o nome dos criterios nao removidos
     */
    public static String[] registarNRemovidos(String[][] nomesCA,
            String[] noNRem, int nNaoRem) {
        String[] nomesNRemovidos = new String[nNaoRem];
        int inseridos = 0;
        for (int i = 0; i < nomesCA[0].length; i++) {
            for (int j = 0; j < nNaoRem; j++) {
                if (nomesCA[0][i].equals(noNRem[j])) {
                    nomesNRemovidos[inseridos++] = nomesCA[0][i];
                    break;
                }
            }
        }
        return nomesNRemovidos;
    }

    /**
     * Metodo que verifica quais sao os nomes diferentes e grava num vetor
     *
     * @param nomesCA matriz dos nomes dos criterios e alternativas
     * @param nomesNRem nomes que nao foram removidos
     * @param nNaoRem numero de nao removidos
     * @return vetor com nome dos removidos
     *
     * O metodo procura as igualdades entre os nomes todos e regista os
     * diferentes num vetor de acordo com o numero de removidos
     */
    public static String[] registarRemovidos(String[][] nomesCA,
            String[] nomesNRem, int nNaoRem) {

        String[] nomesRemovidos = new String[nomesCA[0].length - nNaoRem];
        int inseridos = 0;
        for (int i = 0; i < nomesCA[0].length; i++) {
            for (int j = 0; j < nNaoRem; j++) {
                if (nomesCA[0][i].equals(nomesNRem[j])) {
                    break;
                } else if (j == nNaoRem - 1) {
                    nomesRemovidos[inseridos] = nomesCA[0][i];
                    inseridos += 1;
                }
            }
        }
        return nomesRemovidos;
    }

    /**
     * Remove a matriz do criterio correspondente
     *
     * @param matriz matriz para remover um criterio
     * @param indice indice a remover o criterio
     * @return matriz com criterio removido
     */
    public static double[][][] removerElementos3D(double[][][] matriz,
            int indice) {
        int tamMatriz = matriz[0].length;
        double[][][] novaMatriz
                = new double[matriz.length - 1][tamMatriz][tamMatriz];

        for (int i = 0; i < novaMatriz.length; i++) {
            novaMatriz[i] = copiaMatriz(matriz[i]);
        }

        for (int i = indice; i < novaMatriz.length; i++) {
            novaMatriz[i] = novaMatriz[i++];
        }
        return novaMatriz;
    }

    /**
     * Metodo para ler o ficheiro AHP
     *
     * @param nCriAlt numero de criterios e alternativas
     * @param matrizesA matriz que contem a matriz referente a cada criterio
     * @param matrizC matriz dos criterios
     * @param nomesCA matriz dos nomes dos criterios e alternativas
     * @param nomeF nome do ficheiro de entrada
     * @throws FileNotFoundException
     */
    public static void lerFicheiro(int[] nCriAlt, double[][][] matrizesA,
            double[][] matrizC, String[][] nomesCA, String nomeF)
            throws FileNotFoundException {

        int contTabela = 0;
        int numLinhaCrit = 0;
        int numLinhaAlt = 0;
        File ficheiro = new File(nomeF);
        if (ficheiro.exists()) {
            Scanner lerF = new Scanner(ficheiro);
            while (lerF.hasNext()) {
                String[] linha = lerF.nextLine().split(" +");
                if (linha.length > 0) {
                    if (!linha[0].isEmpty() && isDouble(linha[0])) {
                        double[] linhaDouble = convertArrayStringParaDouble(linha);
                        if (matrizC[nCriAlt[0] - 1][nCriAlt[0] - 1] == 0) {
                            matrizC[numLinhaCrit] = linhaDouble;
                            numLinhaCrit++;
                        } else if (numLinhaAlt < nCriAlt[1]) {
                            matrizesA[contTabela][numLinhaAlt]
                                    = linhaDouble;
                            numLinhaAlt++;
                        } else {
                            numLinhaAlt = 0;
                            contTabela++;
                            matrizesA[contTabela][numLinhaAlt] = linhaDouble;
                            numLinhaAlt++;
                        }
                    } else if (!linha[0].isEmpty() && !isDouble(linha[0])) {
                        if (nomesCA[0][0] == null) {
                            for (int i = 1; i < linha.length; i++) {
                                nomesCA[0][i - 1] = linha[i];
                            }
                        } else {
                            for (int i = 1; i < linha.length; i++) {
                                nomesCA[1][i - 1] = linha[i];
                            }
                        }
                    }
                }
            }
        } else {
            System.out.println("Ficheiro " + nomeF + " nao existente!");
            System.exit(0);
        }
    }

    /**
     * Apresenta a alternativa melhor de acordo com o vetor composto
     *
     * @param vetor
     * @param matrizNomes
     * @return
     */
    private static String melhorAlternativa(double[] vetor,
            String[][] matrizNomes) {
        int maiorIndex = 0;
        double maiorDouble = 0;
        for (int i = 0; i < vetor.length; i++) {
            if (vetor[i] > maiorDouble) {
                maiorDouble = vetor[i];
                maiorIndex = i;
            }
        }
        return "A alternativa escolhida é: " + matrizNomes[1][maiorIndex];
    }

    /**
     * Metodo que conta quantos criterios e alternativas existem num ficheiro
     *
     * @param nomeF nome do ficheiro de entrada
     * @return numero de criterios e numero de alternativas
     * @throws FileNotFoundException
     */
    public static int[] contarCriteriosAtributos(String nomeF)
            throws FileNotFoundException {
        int numCriterios = 0;
        int numAlternativas = 0;
        File ficheiro = new File(nomeF);
        if (ficheiro.exists()) {
            Scanner lerF = new Scanner(ficheiro);
            while (lerF.hasNext()) {
                String[] linha = lerF.nextLine().split(" +");
                if (linha.length > 0) {
                    if (!linha[0].isEmpty()
                            && linha[0].equalsIgnoreCase("mc_criterios")
                            && !isDouble(linha[0])) {
                        numCriterios = linha.length - 1;
                    } else if (!linha[0].isEmpty() && !isDouble(linha[0])) {
                        numAlternativas = linha.length - 1;
                    }
                }
            }
        }
        int[] criAlt = {numCriterios, numAlternativas};
        return criAlt;
    }

    /**
     * Metodo que trata da conversao de String para double
     *
     * @param linha linha a converter para double
     * @return vetor com valores convertidos para double
     */
    public static double[] convertArrayStringParaDouble(String[] linha) {
        double[] arrayDouble = new double[linha.length];
        for (int i = 0; i < linha.length; i++) {
            String[] fraccao = linha[i].split("/");
            if (fraccao.length == 1) {
                arrayDouble[i] = Double.parseDouble(linha[i]);
            } else {
                double elem1 = Double.parseDouble(fraccao[0]);
                double elem2 = Double.parseDouble(fraccao[1]);
                arrayDouble[i] = elem1 / elem2;
            }
        }
        return arrayDouble;
    }

    /**
     * Metodo que verifica se o elemento a testar e double
     *
     * @param string elemento a testar se e double
     * @return
     */
    public static boolean isDouble(String string) {
        try {
            Double.parseDouble(string);
        } catch (NumberFormatException e) {
            String[] fraccao = string.split("/");
            if (fraccao.length > 1) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * Escreve toda a informacao para um ficheiro determinado pelo utilizador
     * com as matrizes normalizadas
     *
     * @param op opcao escolhida pelo utilizador
     * @param nomeFE nome do ficheiro introduzido pelo utilizador
     * @param matrizC matriz criterios
     * @param vMaxCICR valores maximos, ci e cr para todas as matrizes
     * @param matrizesA matrizes de entrada de cada criterio
     * @param priComp vetor final das alternativas prioritarias
     * @param cMatrizC copia da matriz criterios normalizada
     * @param priC vetor proprio dos criterios
     * @param matrizAlt matriz com vetores prioridades das matrizes de cada
     * criterio
     * @param nomesCA nomes dos criterios e das alternativas
     * @param cMatrizesA matriz normalizada de cada criterio
     * @param cRemov nomes dos criterios removidos
     * @param naoRemov nomes dos criterios nao removidos
     * @throws FileNotFoundException
     */
    public static void escrever(int op, String nomeFE, String[][] nomesCA,
            double[][] matrizC, double[][][] matrizesA, double[][] vMaxCICR,
            double[] priComp, double[] priC, double[][] matrizAlt,
            double[][] cMatrizC, double[][][] cMatrizesA, String[] cRemov,
            String[] naoRemov)
            throws FileNotFoundException {

        try (Formatter write = new Formatter(new File(nomeFE))) {
            if (op == 2) {
                write.format("AHP Aproximado%n%n");
                if (cRemov.length > 0) {
                    write.format("Criterios Removidos: ");
                    for (int i = 0; i < cRemov.length; i++) {
                        write.format(" %s;", cRemov[i]);
                    }
                    write.format("%n%n");
                }
            } else {
                write.format("AHP Exato%n%n");
                if (cRemov.length > 0) {
                    write.format("Criterios Removidos: ");
                    for (int i = 0; i < cRemov.length; i++) {
                        write.format(" %s;", cRemov[i]);
                    }
                    write.format("%n%n");
                }
            }
            System.out.printf("%nMatrizes de entrada:%n");
            System.out.println("Criterios");
            apresentaMatrizes(matrizC);
            for (int i = 0; i < matrizesA.length; i++) {
                System.out.println(nomesCA[0][i]);
                apresentaMatrizes(matrizesA[i]);
            }

            if (op == 2) {
                if (cRemov.length > 0) {
                    writerM(write, "Criterios Normalizado", cMatrizC, naoRemov);
                    writerV(write, "Vetor Criterios", priC);
                    writerT(write, 0, vMaxCICR);
                    for (int i = 0; i < cMatrizesA.length; i++) {
                        writerM(write, naoRemov[i], cMatrizesA[i], nomesCA[1]);
                        writerV(write, "Vetor " + naoRemov[i], matrizAlt[i]);
                        writerT(write, i + 1, vMaxCICR);
                    }
                } else {
                    writerM(write, "Criterios Normalizado", cMatrizC, nomesCA[0]);
                    writerV(write, "Vetor Criterios", priC);
                    writerT(write, 0, vMaxCICR);

                    for (int i = 0; i < cMatrizesA.length; i++) {
                        writerM(write, nomesCA[0][i] + " Normalizado",
                                cMatrizesA[i], nomesCA[1]);
                        writerV(write, "Vetor " + nomesCA[0][i], matrizAlt[i]);
                        writerT(write, i + 1, vMaxCICR);
                    }
                }
            } else if (cRemov.length > 0) {
                writerV(write, "Vetor Criterios", priC);
                writerT(write, 0, vMaxCICR);
                for (int i = 0; i < naoRemov.length; i++) {
                    writerV(write, "Vetor" + naoRemov[i], matrizAlt[i]);
                    writerT(write, i + 1, vMaxCICR);
                }
            } else {
                writerV(write, "Vetor Criterios", priC);
                writerT(write, 0, vMaxCICR);
                for (int i = 0; i < matrizAlt.length; i++) {
                    writerV(write, "Vetor " + nomesCA[0][i], matrizAlt[i]);
                    writerT(write, i + 1, vMaxCICR);

                }
            }
            write.format("%nMatrizes de entrada:%n%n");
            writerM(write, "Criterios", matrizC, nomesCA[0]);
            for (int i = 0; i < matrizesA.length; i++) {
                write.format("%n");
                writerM(write, nomesCA[0][i], matrizesA[i], nomesCA[1]);
            }

            write.format("%nVetor Composto:%n");
            System.out.println("Vetor Composto:");
            for (int i = 0; i < priComp.length; i++) {
                write.format("%-10.2f", priComp[i]);
                System.out.printf("%-10.2f", priComp[i]);
            }
            System.out.printf("%n");
            write.format("%n");

            System.out.printf("%n%s%n", melhorAlternativa(priComp, nomesCA));
            write.format("%n%s%n", melhorAlternativa(priComp, nomesCA));
        }

    }

    /**
     * Metodo que pode ser chamado quando as matrizes normalizadas nao estao
     * disponiveis ou nao foram calculadas
     *
     * @param op opcao escolhida pelo utilizador
     * @param nomeFE nome do ficheiro introduzido pelo utilizador
     * @param matrizC matriz criterios
     * @param vMaxCICR valores maximos, ci e cr para todas as matrizes
     * @param matrizesA
     * @param vComp vetor final das alternativas prioritarias
     * @param nomesCA nomes dos criterios e das alternativas
     * @param priC vetor proprio dos criterios
     * @param matrizAlt matriz com vetores prioridades das matrizes de cada
     * criterio
     * @param cRemovidos
     * @throws FileNotFoundException
     */
    public static void escrever(int op, String nomeFE, String[][] nomesCA,
            double[][] matrizC, double[][][] matrizesA, double[][] vMaxCICR,
            double[] vComp, double[] priC, double[][] matrizAlt,
            String[] cRemovidos, String[] naoRemovidos)
            throws FileNotFoundException {

        double[][][] nullMatrix0 = new double[0][0][0];
        double[][] nullMatrix1 = new double[0][0];

        escrever(op, nomeFE, nomesCA, matrizC, matrizesA, vMaxCICR, vComp, priC,
                matrizAlt, nullMatrix1, nullMatrix0, cRemovidos, naoRemovidos);
    }

    /**
     * Escreve os valores proprios maximos, e os calculos do indice de
     * consistencia e a razao de consistencia
     *
     * @param write objeto formatter
     * @param linha linha da matriz dos valores a ler
     * @param matriz matriz dos valores maximos, CI e CR
     */
    private static void writerT(Formatter write, int linha, double[][] matriz) {
        write.format("%n%nValor Maximo:%-10.2f", matriz[linha][0]);
        write.format("CI:%-10.2f", matriz[linha][1]);
        write.format("CR:%-10.2f%n%n", matriz[linha][2]);
    }

    /**
     * Escreve para ficheiro uma matriz determinada bem como o nome da mesma
     *
     * @param write objeto formatter
     * @param nome nome da matriz
     * @param matriz matriz com valores normalizados
     * @param carAlt matriz com os nomes das alternativas e dos criterios
     * @param nLinhaNomes numero da linha a ser lida da matriz carAlt
     */
    private static void writerM(Formatter write, String nome, double[][] matriz,
            String[] nomesA) {
        write.format("%s:%n", nome);
        for (int i = 0; i < matriz.length; i++) {
            write.format("%-15s", nomesA[i]);
            for (int j = 0; j < matriz.length; j++) {
                write.format("%-5.2f", matriz[i][j]);
            }
            write.format("%n");
        }
        write.format("%n");
    }

    /**
     * Escreve para ficheiro um vetor determinado bem como o nome do mesmo
     *
     * @param write objeto formatter
     * @param nome nome da matriz
     * @param vetor vetor proprio de uma matriz
     */
    private static void writerV(Formatter write, String nome, double[] vetor) {
        write.format("%s:%n", nome);
        for (int i = 0; i < vetor.length; i++) {
            write.format("%-10.2f", vetor[i]);
        }
    }

    /**
     * Apresenta a matriz desejada
     *
     * @param matriz
     */
    private static void apresentaMatrizes(double[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                System.out.printf("%-10.2f", matriz[i][j]);
            }
            System.out.printf("%n");
        }
        System.out.println("");
    }

    /**
     * Verifica a consistencia de todas as matrizes
     *
     * @param vMaxCICR matriz com os valores proprios, indice de consistencia e
     * razao de consistencia
     * @param limiarRC limiar definido que a razao nao pode ultrapassar, por
     * default 0.1
     * @return true se as matrizes estiverem consistentes de acordo com o limiar
     * definido, false se o contrario
     */
    public static boolean verificarConsistencia(double[][] vMaxCICR,
            double limiarRC, String[][] nomes) {
        for (int i = 0; i < vMaxCICR.length; i++) {
            if ((vMaxCICR[i][2] > limiarRC) || (vMaxCICR[i][2] < 0)) {
                if (i == 0) {
                    System.out.println("Matriz criterios inconsistente!");
                } else {
                    System.out.println("Matriz " + nomes[0][i - 1]
                            + " inconsitente!");
                }
                return false;
            }
        }
        return true;
    }

}
