package LAPR1_Final_Project;

import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;

/**
 *
 * @author Vitor Melo
 * @author Vitor Cardoso
 * @author Filipe Ferreira
 * @author Guilherme Oliveira
 */
public class Testes {

    private static final double[][] MATRIZ = {
        {1, 2},
        {3, 4}};
    private static final double[][] MATRIZN = {
        {0.25, 0.3333333333333333},
        {0.75, 0.6666666666666666}};

    private static final int[] CALT = {2, 2};

    //AHP
    public static boolean test_multiplicarMV(double[][] matriz) {
        double[] vetor = {1, 2};
        double[] vetorR = Common.multiplicarMV(matriz, vetor);

        return vetorR.length == 2 && vetorR[0] == 5 && vetorR[1] == 11;
    }

    public static boolean test_transporMatriz(double[][] matriz) {
        double[][] matrizR = Common.transporMatriz(matriz);

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (matriz[i][j] != matrizR[j][i]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean test_normalizaMatrizes() {
        double[][] matriz = {
            {1, 2},
            {3, 4}};
        AproxAHP.normalizaMatrizes(matriz);
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (matriz[i][j] != MATRIZN[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean test_removerElementos2D() {
        double[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int indice = 1;
        double[][] matrizEsperada = {{1, 3}, {7, 9}};
        matriz = Common.removerElementos2D(matriz, indice);

        if (matriz.length != 2) {
            return false;
        }

        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz.length; j++) {
                if (matriz[i][j] != matrizEsperada[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean test_calculaVetorPrioridades(double[][] matriz) {
        double[] vetor = {1.5, 3.5};
        double[] vetorR = AproxAHP.calculaVetorPrioridades(matriz);

        return !(vetor[0] != vetorR[0] || vetor[1] != vetorR[1]);
    }

    public static boolean test_calcularVMax() {
        double[] vetor = {1, 2};
        return 5.25 == AproxAHP.calcularVMax(MATRIZ, vetor);
    }

    public static boolean test_converterMatrixParaDouble(Matrix matriz) {
        double[][] matrizDouble = ExactAHP.converterMatrixParaDouble(matriz);
        for (int i = 0; i < matrizDouble.length; i++) {
            for (int j = 0; j < matrizDouble[0].length; j++) {
                if (MATRIZ[i][j] != matrizDouble[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    // NOTA: o valor proprio é o maior elemento da matriz
    public static boolean test_getValorProprioEPos(double[][] matriz) {
        double[] valorEPos = {4, 1};
        double[] result = ExactAHP.getValorProprioEPos(matriz);
        for (int i = 0; i < valorEPos.length; i++) {
            if (valorEPos[i] != result[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean test_getVetorProprio() {
        double[] vetorProprio = {0.3333333333333333, 0.6666666666666666};
        double[] result = ExactAHP.getVetorProprio(MATRIZ, 1);
        for (int i = 0; i < vetorProprio.length; i++) {
            if (vetorProprio[i] != result[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean test_trataVetorProprio() {
        double[] vetor = {-1, -2, -3};
        double[] vetorTratado = {1, 2, 3};
        double[] result = ExactAHP.trataVetorProprio(vetor);
        for (int i = 0; i < vetor.length; i++) {
            if (vetorTratado[i] != result[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean test_retirarVetorProprio() {
        //primeiro elemento e o vetor proprio
        double[] vetor = {17, 1, 2, 3, 4};
        double[] vetorProprio = {1, 2, 3, 4};
        double[] result = ExactAHP.retirarVetorProprio(vetor);
        for (int i = 0; i < vetorProprio.length; i++) {
            if (vetorProprio[i] != result[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean test_juntarVetorEmMatriz() {
        double[] vetor1 = {1, 2, 3};
        double[] vetor2 = {4, 5, 6};
        double[] vetor3 = {7, 8, 9};
        double[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        double[][] result = ExactAHP.juntarVetorEmMatriz(matriz, vetor1, vetor2, vetor3);
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                if (matriz[i][j] != result[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean test_calculoValorEVetorProprio() {
        double[][] matriz = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        double[] valorEVetorProprio = {16.116843969807057, 0.14719267169882638, 0.3333333333333334, 0.5194739949678402};
        double[] result = ExactAHP.calculoValorEVetorProprio(matriz);
        for (int i = 0; i < matriz.length; i++) {
            if (valorEVetorProprio[i] != result[i]) {
                return false;
            }
        }
        return true;
    }

    //TOPSIS
    public static boolean test_matrizCriteriosAoQuadrado(double[][] matriz, int[] cAlt) {
        double[][] matrizquadrada = {{1, 4}, {9, 16}};
        double[][] matrizCriteriosAoQuadrado
                = Topsis.matrizCriteriosAoQuadrado(matriz, cAlt);
        for (int i = 0; i < matrizCriteriosAoQuadrado.length; i++) {
            for (int j = 0; j < matrizCriteriosAoQuadrado[0].length; j++) {
                if (matrizCriteriosAoQuadrado[i][j] != matrizquadrada[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean test_raizSomaColuna() {
        double[][] matrizquadrada = {{1, 4}, {9, 16}};
        double[] resultTest = {3.1622776601683795, 4.47213595499958};
        double[] result = Topsis.raizSomaColuna(matrizquadrada, CALT);
        for (int i = 0; i < result.length; i++) {
            if (resultTest[i] != result[i]) {
                return false;
            }
        }
        return true;
    }

    public static boolean test_normalizaTOPSIS() {
        double[][] matriznormalizada = {{0.31622776601683794, 0.4472135954999579},
        {0.9486832980505138, 0.8944271909999159}};
        double[] resultTest = {3.1622776601683795, 4.47213595499958};
        double[][] result = Topsis.normalizaTOPSIS(MATRIZ, CALT, resultTest);
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result.length; j++) {
                if (result[i][j] != matriznormalizada[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean test_matrizPesada() {
        double[][] matriznormalizada = {{0.31622776601683794, 0.4472135954999579},
                {0.9486832980505138, 0.8944271909999159}};
        double[] pesos = {0.25, 0.25, 0.25, 0.25};
        double[][] matrizPesada = {{0.07905694150420949, 0.11180339887498948},
                {0.23717082451262844, 0.22360679774997896}};
        double[][] result = Topsis.matrizPesada(matriznormalizada, pesos);
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result.length; j++) {
                if (result[i][j] != matrizPesada[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {

        System.out.println("\nAproxAHP:");
        System.out.println("Teste multiplicarMV: "
                + test_multiplicarMV(MATRIZ));
        System.out.println("Teste transporMatriz: "
                + test_transporMatriz(MATRIZ));
        System.out.println("Teste normalizaMatrizes: "
                + test_normalizaMatrizes());
        System.out.println("Teste calculaVetorPrioridades: "
                + test_calculaVetorPrioridades(MATRIZ));
        System.out.println("Teste calcularVMax: "
                + test_calcularVMax());
        System.out.println("Teste removerElemento2D "
                + test_removerElementos2D());

        System.out.println("\n\nExactAHP:");
        Matrix matrizLib = new Basic2DMatrix(MATRIZ);
        System.out.println("Testar calculoValorEVetorProprio: "
                + test_calculoValorEVetorProprio());
        System.out.println("Testar converterMatrixParaDouble: "
                + test_converterMatrixParaDouble(matrizLib));
        System.out.println("Testar getValorProprioEPos: "
                + test_getValorProprioEPos(MATRIZ));
        System.out.println("Testar getVetorProprio: "
                + test_getVetorProprio());
        System.out.println("Testar trataVetorProprio: "
                + test_trataVetorProprio());
        System.out.println("Testar retirarVetorProprio: "
                + test_retirarVetorProprio());
        System.out.println("Testar juntarVetorEmMatriz: "
                + test_juntarVetorEmMatriz());

        System.out.println("\n\nTOPSIS");
        System.out.println("Testar matrizCriteriosAoQuadrado: "
                + test_matrizCriteriosAoQuadrado(MATRIZ, CALT));
        System.out.println("Testar raizSomaColuna: "
                + test_raizSomaColuna());
        System.out.println("Testar normalizaTOPSIS: "
                + test_normalizaTOPSIS());
        System.out.println("Testar matrizPesada: "
                + test_matrizPesada());

    }

}
