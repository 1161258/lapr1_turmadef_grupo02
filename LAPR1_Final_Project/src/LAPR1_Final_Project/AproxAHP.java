package LAPR1_Final_Project;

/**
 *
 * @author Vitor Melo
 * @author Vitor Cardoso
 * @author Filipe Ferreira
 * @author Guilherme Oliveira
 */
public class AproxAHP {

    /**
     * Normaliza a matriz ao dividir cada elemento pela soma da coluna
     *
     * @param matriz matriz de entrada desejada para normalizar
     */
    public static void normalizaMatrizes(double[][] matriz) {
        double soma;
        for (int j = 0; j < matriz.length; j++) { //coluna
            soma = somaColuna(matriz, j);
            for (int i = 0; i < matriz.length; i++) { //linha
                matriz[i][j] = matriz[i][j] / soma;
            }
        }
    }

    /**
     * Faz a soma de todos os elementos de cada coluna
     *
     * @param matriz matriz de entrada
     * @param coluna coluna desejada a somar
     * @return
     */
    private static double somaColuna(double[][] matriz, int coluna) {
        double soma = 0;
        for (int i = 0; i < matriz.length; i++) { //linha
            soma += matriz[i][coluna];
        }
        return soma;
    }

    /**
     * Calcula a média de cada linha de uma determinada matriz e retorna os
     * valores atraves de um vetor
     *
     * @param matriz
     * @return
     */
    public static double[] calculaVetorPrioridades(double[][] matriz) {
        double[] vetorPrior = new double[matriz.length];
        double soma;
        for (int i = 0; i < matriz.length; i++) {
            soma = 0;
            for (int j = 0; j < matriz[i].length; j++) {
                soma += matriz[i][j];
            }
            vetorPrior[i] = soma / matriz[i].length;
        }
        return vetorPrior;
    }

    /**
     * Calcula valor proprio maximo
     *
     * @param matriz matriz normalizada de entrada
     * @param vetor vetor de prioridades da matriz de entrada
     * @return
     */
    public static double calcularVMax(double[][] matriz, double[] vetor) {
        double[] vetorMV = Common.multiplicarMV(matriz, vetor);
        //vetorMV vetor resultante da multiplicacao da matriz pelo vetor
        double soma = 0;
        if (vetor.length == vetorMV.length) {
            for (int i = 0; i < vetorMV.length; i++) {
                soma += (vetorMV[i] / vetor[i]);
            }
        }
        return soma / vetor.length;
    }

    /**
     * Metodo para preencher a matriz vMaxCICR com os valores proprios maximos,
     * o indice de consistencia e a razao de consistencia
     *
     * @param vMaxCICR matriz vMaxCICR
     * @param matriz matriz de entrada nao normalizada
     * @param vetor vetor prioridades relativo a matriz nao normalizada
     * @param i posicao para guardar os valores na matriz vMaxCICR
     */
    public static void preencheVMaxCICR(double[][] vMaxCICR, double[][] matriz,
            double[] vetor, int i) {
        vMaxCICR[i][0] = calcularVMax(matriz, vetor);
        vMaxCICR[i][1] = Common.calculaCI(vMaxCICR[i][0], matriz.length);
        vMaxCICR[i][2] = Common.calculaCR(vMaxCICR[i][1], matriz.length);
    }

    /**
     * Calcula o vetor prioridades para cada matriz e introduz os vetores numa
     * matriz de alternativas
     *
     * @param vetoresAlt matriz alternativas
     * @param cMatrizesA
     */
    public static void copiaVetoresAlternativas(double[][] vetoresAlt,
            double[][][] cMatrizesA) {

        for (int i = 0; i < cMatrizesA.length; i++) {
            double[] aux = calculaVetorPrioridades(cMatrizesA[i]);
            System.arraycopy(aux, 0, vetoresAlt[i], 0, aux.length);
        }
    }
}
