# LAPR1_TurmaDEF_Grupo02 #
***
## Projeto de LAPR1 ##

Aqui irá ficar o código e os ficheiros necessários para o projeto.

Para aceder às classes que contêm o código da aplicação deverá ir à pasta:
> */LAPR1_Final_Project/src/LAPR1_Final_Project*

Para aceder ao programa já compilado deverá aceder à pasta: 
> */LAPR1_Final_Project/dist

Para executar o programa na linha de comandos deverá digitar o seguinte: 
> *java -jar LAPR1_Final_Project.jar input.txt output.txt*

>> Opções permitidas:

>>      -M [1|2], escolha do metodo a ser usado (1 AHP - 2 TOPSIS)

>> Se -M 2 a opcao -L e -S se introduzidas serao ignoradas

>>      -L [>= 0] escolha do limiar a ser usado no método AHP

>> Se -L nao for invocado o limiar sera por default 0

>>      -S [>= 0.1] escolha do limiar para a Razao de Consistencia (RC)

Se a opção **-m** não for encontrada o programa não fará nenhum output dado que o método não foi escolhido.

Deverá de ter obrigatóriamente dois nomes de ficheiros, caso contrário a aplicação não poderá realizar nenhuns cálculos